const fileDB = require("./fileDB");
const newspostSchema = {
    id: Number,
    title: String,
    text: String,
    createDate: Date,
};

fileDB.registerSchema('newspost', newspostSchema);

const newspostTable = fileDB.getTable('newspost');

// повертаємо усі записи у базі у вигляді масиву
function newsposts() {
    return newspostTable.getAll();
}

function typeingOfId(value) {
    let type = newspostSchema.id;
    return type(value);
}

// повертаємо запис за вказаним id
function newspost(id) {
    return newspostTable.getById(typeingOfId(id));
}

// додаємо новий запис, та повертаємо його з новим id
function createdNewspost(data) {
    return newspostTable.create(data);
}

// оновлюємо поле title за вказаним id та повертаємо оновлений запис
function updatedNewsposts(id, data) {
    return newspostTable.update(id, data);
}

// видаляємо записа за вказаним id та повертаємо id видаленого запису
function deletedById(id) {
    return newspostTable.deletedById(id)
}

module.exports = {
    newsposts: newsposts,
    newspost: newspost,
    createdNewspost: createdNewspost,
    updatedNewsposts: updatedNewsposts,
    deletedById: deletedById
};