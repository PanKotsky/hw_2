const fs = require('fs');
const path = require('path');

let tables = {};
function registerSchema(name, schema) {
    tables[name] = schema;
}
function getTable(tableName) {
    return {
        getAll: () => {
            return read(tableName);
        },
        getById: (id) => {
            return read(tableName, id);
        },
        create: function (data) {
            return write(tableName, data);
        },
        update: function (id, data) {
            return updateById(tableName, id, data);
        },
        deletedById: function (id) {
            return deleteById(tableName, id);
        },
    };
}

function read(filename, id = null) {
    try {
        filename = getFilename(filename);
        if (!fs.existsSync(filename)) {
            throw new Error(`Table ${tableName} doesn't exist`);
        }
        let data = prepareDataRead(fs.readFileSync(filename, 'utf8'));

        if (id) {
            data = data.find(value => value.id === id);
        }

        return data;

    } catch (err) {
        console.error('Can`t read resource of file db', err.message);
    }
}

function write(table, newRecord) {
    try {
        let filename = getFilename(table);
        !checkFile(filename) && fs.writeFileSync(filename, '');
        let data = read(table) ?? [];
        let lastRecord = getLastRecord(data);
        let newRecordId = lastRecord ? ++lastRecord.id : 1;
        newRecord = hydrateValue(table, newRecord, newRecordId);
        fs.writeFileSync(filename, prepareDataWrite(data, newRecord));
        return newRecord;
    } catch (e) {
        console.error(e.message);
    }
}

function deleteById(table, id) {
    try {
        if (id) {
            throw new Error('Record ID is not specified');
        }

        let result = null;
        let filename = getFilename(table);

        if (!checkFile(filename)) {
            return null;
        }

        let data = read(table);
        let index = data.findIndex(value => value.id === Number(id));

        if (index !== -1) {
            data.splice(index, 1);
            fs.writeFileSync(filename, JSON.stringify(data));
            result = id;
        }

        return result;
    } catch (e) {
        console.error(e.message);
    }
}

function updateById(table, id, data) {
    try {
        if (id) {
            throw new Error('Record ID is not specified');
        }

        let result = null;
        let filename = getFilename(table);
        if (!checkFile(filename)) {
            return null;
        }

        let records = read(table);
        let index = records.findIndex(value => value.id === Number(id));
        if (index !== -1) {
            records[index] = result = hydrateValue(table, data);
            fs.writeFileSync(filename, JSON.stringify(records));
        }

        return result;
    } catch (e) {
        console.error(e.message);
    }
}

function getFilename(tableName) {
    return  path.join(__dirname, 'db/' + tableName+'.txt');
}

function checkFile(filename) {
    return fs.existsSync(filename);
}

function prepareDataRead(data)
{
    if (typeof data === 'string' ) {
        return data ? JSON.parse(data) : [];
    } else {
        throw new Error('Unexpected format of data');
    }
}

function prepareDataWrite(data, newRecord) {
    data.push(newRecord);

    if (typeof data === 'object') {
        return JSON.stringify(data);
    } else {
        console.error(data);
        throw new Error('Unexpected format of data');
    }
}
function hydrateValue(table, newRecord, newId = null) {
    let record = {};
    for ([key, value] of Object.entries(tables[table])) {
        if (key === 'id') {
            record[key] = newId || newRecord[key];
        }  else if (value === Date) {
            record[key] = new Date().toISOString();
        } else {
            record[key] = value(newRecord[key]);
        }
    }

    return record;
}

function getLastRecord(data)
{
    return data.length > 0 ? data[data.length - 1] : null;
}

module.exports = {
    registerSchema: registerSchema,
    getTable: getTable
}
