const model = require("./src/model");

// додаємо новий запис, та повертаємо його з новим id
const data = {
    title: 'У зоопарку Чернігова лисичка народила лисеня',
    text: "В Чернігівському заопарку сталася чудова подія! Лисичка на ім'я Руда народила чудове лисенятко! Тож поспішайте навідатись та подивитись на це миле створіння!"
}

const options = process.argv.reduce((acc, cur) => {
    if (cur.includes('--')) {
        const paramValue = cur.split('=');
        acc['func'] = paramValue[0].replace('--', '');
        acc['param'] = paramValue[1];
    }
    return acc;
}, {});

const func = options.func;
const id = options.param;
let result;

switch (func) {
    case 'newsposts':
        result = model.newsposts();
        break;
    case 'newspost':
        result = model.newspost(id);
        break;
    case 'createdNewspost':
        result = model.createdNewspost(data);
        break;
    case 'updatedNewsposts':
        result = model.updatedNewsposts(id, {title: "Маленька лисичка"});
        break;
    case 'deletedById':
        result = model.deletedById(id);
        break;
    default:
        throw new Error('The action doesn\'t allowed');
}

console.log(result);